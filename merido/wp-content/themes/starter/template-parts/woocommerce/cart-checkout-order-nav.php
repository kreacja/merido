<div class="cart-checkout-order-nav">
    <p <?php echo $section == "cart" ? "class='active'" : ""; ?>><?= __('Koszyk', 'starter'); ?></p>
    <p <?php echo $section == "checkout" ? "class='active'" : ""; ?>><?= __('Zamówienie', 'starter'); ?></p>
    <p <?php echo $section == "order" ? "class='active'" : ""; ?>><?= __('Potwierdzenie', 'starter'); ?></p>
</div>