<?php
$qo = get_queried_object(); ?>
<nav class="sidebar-categories-list">
    <?php if (is_shop()) : ?>

        <?php
        $terms = get_terms([
            "taxonomy" => "product_cat",
            "hide_empty" => false
        ]);
        if ($terms) {
            foreach ($terms as $term) : ?>
                <?php if ($term->parent == 0) : ?>
                    <a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
                <?php endif; ?>
        <?php endforeach;
        }
        ?>

    <?php elseif (is_product_category() && $qo->parent == 0) : ?>

        <a href="<?php echo get_permalink(wc_get_page_id('shop')); ?>" class="up-link"><?= __('&laquo Sklep', 'starter'); ?></a>

        <?php
        $children = get_terms([
            "taxonomy" => "product_cat",
            "hide_empty" => false,
            "child_of" => $qo->term_id,
            "orderby" => "name"
        ]);
        foreach ($children as $child) : ?>
            <?php $term = get_term($child); ?>
            <a href="<?php echo get_term_link($term) ?>"><?php echo $term->name; ?> <span>(<?php echo $term->count; ?>)</span></a>
        <?php endforeach; ?>

    <?php elseif (is_product_category() && $qo->parent != 0) : ?>

        <?php $parent = get_term($qo->parent); ?>
        <a href="<?php echo get_term_link($parent) ?>" class="up-link">&laquo; <?php echo $parent->name; ?></a>

        <?php
        $remaining_children = get_terms([
            "taxonomy" => "product_cat",
            "child_of" => $parent->term_id,
            "hide_empty" => false,
            "orderby" => "name"
        ]);
        if (isset($remaining_children) && !empty($remaining_children)) : ?>
            <?php foreach ($remaining_children as $term) : ?>
                <a href="<?php echo get_term_link($term); ?>" <?php echo $term->term_id === $qo->term_id ? "class='current'" : "" ?>>
                    <?php echo $term->name; ?> <span>(<?php echo $term->count; ?>)</span>
                </a>
            <?php endforeach; ?>
        <?php endif; ?>

    <?php endif; ?>
</nav>