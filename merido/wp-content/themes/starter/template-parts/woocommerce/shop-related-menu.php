<div class="shop-menu-container">
    <a href="#0" class="open-seach-form-btn">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
            <g id="Szukaj" transform="translate(-1343 -90)">
                <path id="search" d="M6,12a6,6,0,1,1,6-6A6.007,6.007,0,0,1,6,12ZM6,1.333A4.667,4.667,0,1,0,10.667,6,4.672,4.672,0,0,0,6,1.333ZM15.333,16a.665.665,0,0,1-.471-.2L9.529,10.471a.667.667,0,0,1,.943-.943L15.8,14.862A.667.667,0,0,1,15.333,16Z" transform="translate(1343 90)"/>
            </g>
        </svg>
    </a>
    <a href="<?php echo wc_get_page_permalink('myaccount'); ?>">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">
            <path id="profil" d="M12.2,10.535A5.158,5.158,0,0,0,9.546,1a5.158,5.158,0,0,0-2.657,9.535c-3.14.713-5.343,2.662-5.343,5.021,0,1.93,5.029,2.445,8,2.445s8-.515,8-2.445C17.546,13.2,15.343,11.248,12.2,10.535ZM5.508,6.151a4.038,4.038,0,1,1,4.038,4.088A4.068,4.068,0,0,1,5.508,6.151ZM9.546,16.938c-4.271,0-6.828-.914-6.952-1.386,0-2.344,3.121-4.25,6.952-4.25s6.948,1.906,6.952,4.249C16.377,16.023,13.82,16.938,9.546,16.938Z" transform="translate(-1.546 -1)" />
        </svg>
    </a>

    <?php
    echo do_shortcode('[ti_wishlist_products_counter]');
    ?>

    <div class="cart-icon-wrapper">
        <?php get_template_part('template-parts/woocommerce/shop-related-menu-cart'); ?>
    </div>
</div>