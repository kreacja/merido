<div class="add-to-fav-share">
    <?php echo do_shortcode('[ti_wishlists_addtowishlist]'); ?>
    <div class="social-share">
        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16">
            <path id="share" d="M12.333,5.333A2.667,2.667,0,1,1,15,2.667,2.669,2.669,0,0,1,12.333,5.333Zm0-4a1.333,1.333,0,1,0,1.333,1.333A1.335,1.335,0,0,0,12.333,1.333ZM3.667,10.667A2.667,2.667,0,1,1,6.333,8,2.669,2.669,0,0,1,3.667,10.667Zm0-4A1.333,1.333,0,1,0,5,8,1.335,1.335,0,0,0,3.667,6.667ZM12.333,16A2.667,2.667,0,1,1,15,13.333,2.669,2.669,0,0,1,12.333,16Zm0-4a1.333,1.333,0,1,0,1.333,1.333A1.335,1.335,0,0,0,12.333,12ZM5.371,7.619a.666.666,0,0,1-.35-1.234l3.421-2.1a.667.667,0,0,1,.7,1.135L5.719,7.52a.667.667,0,0,1-.349.1Zm4.8,5.048a.667.667,0,0,1-.349-.1L5.484,9.9a.666.666,0,1,1,.7-1.135l4.333,2.667a.666.666,0,0,1-.349,1.234Z" transform="translate(-1)" />
        </svg>

        <div class="social-media-share-links">
            <?php
            $permalink = get_the_permalink();
            $title = get_the_title();
            $fb_link = sprintf("https://www.facebook.com/sharer/sharer.php?u=%s", $permalink);
            $twitter_link = sprintf("https://twitter.com/intent/tweet?text=%s&url=%s", $title, $permalink);
            $linkedin_link = sprintf("https://www.linkedin.com/shareArticle?mini=true&url=%s&title=%s", $permalink, $title);
            ?>
            <a href="<?php echo $fb_link; ?>" target="_blank" class="fb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/facebook.svg" alt="Facebook"></a>
            <a href="<?php echo $twitter_link; ?>" target="_blank" class="twitter"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/twitter.svg" alt="Twitter"></a>
            <a href="<?php echo $linkedin_link; ?>" target="_blank" class="linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/linkedin.svg" alt="LinkedIn"></a>
        </div>
    </div>
</div>