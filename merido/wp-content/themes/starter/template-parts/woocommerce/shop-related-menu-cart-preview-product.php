<?php
$product = wc_get_product($cart_item['data']->get_id());
$product_id = $product->get_id();
if ($product) :
?>
    <div class="single-cart-item">
        <figure>
            <?php
            echo apply_filters('woocommerce_cart_item_thumbnail', $product->get_image(), $cart_item, $cart_key);
            ?>
        </figure>
        <div class="item-info">
            <p class="name"><?php echo $product->get_title(); ?></p>
            <div class="details">
                <p><?= __('Ilość:', 'starter'); ?> <?php echo $cart_item['quantity']; ?><span class="separator">|</span> <?= __('Cena:', 'starter'); ?>&nbsp;<?php echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($product, $cart_item['quantity']), $cart_item, $cart_key); ?></p>
            </div>
        </div>
    </div>
<?php endif; ?>