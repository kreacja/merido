<?php global $product; ?>
<?php if ($product->get_type() === "simple") : ?>
    <a href="#0" class="add-to-cart ajax-add-to-cart" data-ajaxid="<?php echo the_ID(); ?>">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="18" viewBox="0 0 16 18">
            <path id="dodaj-do-koszyka" d="M17.456,5.6a.56.56,0,0,0-.557-.527H13.944v-.88A3.476,3.476,0,0,0,10.25,1,3.476,3.476,0,0,0,6.555,4.195v.88H3.6a.56.56,0,0,0-.557.527L2.251,18.4a.567.567,0,0,0,.15.42A.557.557,0,0,0,2.808,19H17.691a.558.558,0,0,0,.407-.177.567.567,0,0,0,.15-.42ZM7.671,4.195a2.369,2.369,0,0,1,2.578-2.07,2.369,2.369,0,0,1,2.578,2.07v.88H7.671Zm-1.116,2V7.421a.558.558,0,1,0,1.116,0V6.2h5.157V7.421a.558.558,0,1,0,1.116,0V6.2h2.429l.484,7.8H3.642l.484-7.8ZM3.4,17.875,3.6,14.75H16.9l.194,3.125Z" transform="translate(-2.25 -1)" fill="#374b42" />
        </svg>
        <?= __('Dodaj do koszyka', 'starter'); ?>
    </a>
<?php else: ?>
    <a href="<?php echo get_the_permalink( $product ); ?>">
        <?= __('Zobacz produkt', 'starter'); ?>
    </a>
<?php endif; ?>