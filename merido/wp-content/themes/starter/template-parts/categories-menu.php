<?php
$args = [
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'parent' => 0
];
$terms = get_terms($args);
if (isset($terms) && !empty($terms)) : ?>
    <div class="categories-menu-container">
        <div class="wrapper">
            <ul>
                <?php foreach ($terms as $term) : ?>
                    <li>
                        <a href="<?php echo get_term_link($term); ?>">
                            <?php echo $term->name; ?>
                        </a>

                        <?php
                        $children_args = [
                            'taxonomy' => 'product_cat',
                            'hide_empty' => false,
                            'parent' => $term->term_id
                        ];
                        $children = get_terms($children_args);
                        if (isset($children) && !empty($children)) : ?>
                            <ul class="subcategories-menu">
                                <?php foreach ($children as $term) : ?>
                                    <li><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="mobile-view">
            <a role="button" class="show-mobile-category-list"><?= __('Kategorie produktów', 'starter'); ?></a>
            <ul class="mobile-list-container">
                <?php
                wp_list_categories([
                    "taxonomy" => "product_cat",
                    "hide_empty" => false,
                    'title_li' => ""
                ]);
                ?>
            </ul>
        </div>
    </div>
<?php endif;
