<?php $socials = get_field('social_sites_list', 'option'); ?>

<?php if ($socials): ?>
	<div class="social-links-container">
		<?php foreach ($socials as $social): ?>
			<?php if ($social['link'] && $social['icon']): ?>
				<a href="<?php echo $social['link']; ?>" target="_blank" class="social-link"><img src="<?php echo $social['icon']; ?>"></a>
			<?php endif ?>
		<?php endforeach ?>
	</div>
<?php endif ?>