<?php
$args = [
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'parent' => 0
];
$terms = get_terms($args);
if (isset($terms) && !empty($terms)) : ?>

    <div class="categories-tiles-wrapper">

        <?php foreach ($terms as $term) : ?>
            <a href="<?php echo get_term_link($term); ?>">
                <?php $thumbnail_id = get_term_meta($term->term_id, 'thumbnail_id', true); ?>
                <?php $image = wp_get_attachment_url($thumbnail_id); ?>
                <?php if($image): ?>
                    <img src="<?php echo $image; ?>" alt="<?php echo $term->name; ?>">
                <?php endif; ?>
                <p><?php echo $term->name; ?></p>
            </a>
        <?php endforeach; ?>

    </div>

<?php endif; ?>