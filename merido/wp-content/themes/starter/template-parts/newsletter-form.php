<div class="newsletter-form-container">
    <div class="left">
        <h3><?= __('Zapisz się na newsletter, a otrzymasz 15% rabatu na pierwsze zakupy', 'starter'); ?></h3>
    </div>
    <div class="right">
        <form action="">
            <input type="email" placeholder="e-mail">
            <input type="submit" value="<?= __('Zapisz się', 'starter'); ?>">
        </form>
    </div>
    <!-- TODO: Add functionality to handle the form -->
</div>