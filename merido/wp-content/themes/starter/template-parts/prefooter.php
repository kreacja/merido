<div class="prefooter">
	<div class="wrapper">
		<div class="one">
			<figure>
				<a href="<?php echo home_url('/') ?>" rel="home"><img class="image" src="<?php echo get_template_directory_uri(); ?>/dist/images/kreacja.png" alt="<?php bloginfo('name'); ?>"></a>
			</figure>

			<div class="badges">
				<figure class="badge">
					<img src="<?php echo get_template_directory_uri() ?>/dist/images/icons/badge-recommended.svg" alt="">
					<figcaption><?= __('Sklep Polecany Przez Klientów', 'starter'); ?></figcaption>
				</figure>
				<figure class="badge">
					<img src="<?php echo get_template_directory_uri() ?>/dist/images/icons/badge-solid.svg" alt="">
					<figcaption><?= __('Rzetalna<br />firma', 'starter'); ?></figcaption>
				</figure>
				<figure class="badge">
					<img src="<?php echo get_template_directory_uri() ?>/dist/images/icons/badge-secure.svg" alt="">
					<figcaption><?= __('Bezpieczne Płatności', 'starter'); ?></figcaption>
				</figure>
			</div>

			<?php $socials = get_field('social_sites_list', 'option'); ?>
			<?php if ($socials) : ?>
				<div class="social">
					<?php foreach ($socials as $social) : ?>
						<?php if ($social['link'] && $social['icon']) : ?>
							<a href="<?php echo $social['link']; ?>" target="_blank" class="social-link"><img src="<?php echo $social['icon']; ?>"></a>
						<?php endif ?>
					<?php endforeach ?>
				</div>
			<?php endif ?>
		</div>

		<div class="two">
			<h4><?= __('Warunki zakupów', 'starter'); ?></h4>
			<?php
			$pages = get_children([
				'post_parent' => 216
			]);
			if (is_array($pages) && !empty($pages)) : ?>
				<?php foreach ($pages as $page) : ?>
					<a href="<?php echo get_the_permalink($page); ?>"><?php echo $page->post_title; ?></a>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>

		<div class="three">
			<h4><?= __('Twoje konto', 'starter'); ?></h4>
			<a href="<?php echo get_permalink(get_option('woocommerce_cart_page_id')); ?>"><?= __('Twój koszyk', 'starter'); ?></a>
			<a href="<?php echo home_url(); ?>/moje-konto/orders/"><?= __('Twoje zamówienia', 'starter'); ?></a>
			<a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"><?= __('Logowanie', 'starter'); ?></a>
			<a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"><?= __('Rejestracja', 'starter'); ?></a>
		</div>

		<div class="four">
			<h4><?= __('Firma', 'starter'); ?></h4>
			<a href="<?php echo get_the_permalink(457); ?>"><?= __('Kontakt', 'starter'); ?></a>
			<a href="<?php echo get_the_permalink(455); ?>"><?= __('O nas', 'starter'); ?></a>
		</div>
	</div>
</div>