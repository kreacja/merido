

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php the_page_title(); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Gotów, by opublikować pierwszy post? <a href=\"%1$s\">Zacznij tutaj</a>.', 'nazwa' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( 'Brak wyników wyszukiwania. Proszę spróbować ponownie z innymi słowami.', 'nazwa' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( 'Brak wyników dla tej strony. Być może wyszukiwanie przyniesie lepsze rezultaty.', 'nazwa' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
