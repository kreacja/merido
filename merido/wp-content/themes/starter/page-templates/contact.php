<?php

/**
 * Template name: Kontakt
 */
get_header();
the_post();
?>

<div class="banner">
    <h3><?= __('Masz pytanie?', 'starter'); ?></h3>
    <strong><?= __('Sprawdź najczęściej zadawane pytania lub skontaktuj się z nami.', 'starter'); ?></strong>
    <p><?= __('Pracownicy naszego Biura Obsługi Klienta chętnie udzielą odpowiedzi na wszystkie Twoje pytania.', 'starter'); ?></p>
</div>

<div class="faq-form-wrapper">
    <div class="left">
        <?php
        $faqs = get_field('faq');
        if (isset($faqs) && !empty($faqs)) : ?>
            <div class="faqs-container">
                <div class="header">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/faq.svg" alt="">
                    <h3><?= __('Najczęściej zadawane pytania', 'starter'); ?></h3>
                </div>
                <?php foreach ($faqs as $faq) : ?>

                    <a href="#0" class="question">
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/arrow-faq.svg" alt="">
                        <?php echo $faq['question']; ?>
                    </a>
                    <p class="answer"><?php echo $faq['answer']; ?></p>

                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="right">
        <div class="header">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/form.svg" alt="">
            <h3><?= __('Zapytaj przez formularz', 'starter'); ?></h3>
        </div>
        <p><?= __('Wypełnij jeden z naszych formularzy pomocy aby zyskać odpowiedź na Twoje pytania.', 'starter'); ?></p>
        <a href="#form" role="button"><?= __('Wypełnij formularz pomocy', 'starter'); ?></a>
    </div>
</div>

<div class="tiles-wrapper">
    <div class="company-info">
        <p><?php echo get_field('company-name'); ?></p>
        <p><?php echo get_field('street'); ?></p>
        <p><?php echo get_field('postcode-city'); ?></p>
    </div>

    <div class="phone">
        <p><a href="tel:<?php echo get_field('phone'); ?>"><?php echo get_field('phone'); ?></a></p>
    </div>

    <div class="email">
        <p><a href="mailto:<?php echo get_field('mail'); ?>"><?php echo get_field('mail'); ?></a></p>
    </div>
</div>

<div class="form-container" id="form">
    <h3><?= __('Formularz kontaktowy', 'starter'); ?></h3>
    <?php echo do_shortcode('[contact-form-7 id="37" title="Formularz kontaktowy"]'); ?>
</div>


<?php
get_footer();
?>