<?php
/**
 * Template name: Strona rodo
 */

get_header(); 
the_post();?>

	<div id="primary" class="content-area full">
		<main id="main" class="site-main" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php echo do_shortcode('[rodo_kind_of_page]'); ?>

			</article><!-- #post-## -->			

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
