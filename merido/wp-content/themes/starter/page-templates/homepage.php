<?php
/**
 * Template name: Strona główna
 */

get_header(); 
the_post();?>

	<div id="primary" class="content-area full">
		<main id="main" class="site-main" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article><!-- #post-## -->			

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
