<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preload" href="//fonts.googleapis.com/css2?family=Poppins%3Awght%40400%3B500%3B600%3B700&amp;display=swap" as="style" onload="this.rel='stylesheet'">
	<?php wp_head(); ?>

	<?php if (!empty(get_field('head_scripts', 'options'))) echo get_field('head_scripts', 'options'); ?>
</head>

<body <?php body_class(); ?>>
	<?php if (!empty(get_field('body_scripts_top', 'options'))) echo get_field('body_scripts_top', 'options'); ?>

	<div class="search-form-popup">
		<div class="wrapper">
			<?php echo get_search_form(); ?>
			<div class="close-search-form-popup-btn-wrapper">
				<a href="#0" class="close-search-form-popup">X</a>
			</div>
		</div>
		<div class="search-results-wrapper">
			<div class="wrapper">
			</div>
			<div class="s-loader"></div>
		</div>
	</div>

	<div class="popup-container">
		<div class="inner-content-wrapper">
			<div class="inner-content"><?= __('Proszę czekać...', 'starter'); ?>
			</div>
			<!-- <button class="close-popup">X</button> -->
		</div>
	</div>

	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#content"><?php _e('Przejdź do treści', 'nazwa'); ?></a>
		<header id="masthead" class="site-header" role="banner">
			<div class="wrapper">

				<div class="site-branding">
					<a href="<?php echo home_url('/') ?>" rel="home"><img class="image" src="<?php echo get_template_directory_uri(); ?>/dist/images/kreacja.png" alt="<?php bloginfo('name'); ?>"></a>
				</div><!-- .site-branding -->

				<div id="toggle">
					<div class="toggle-container">
						<span class="toggle-item"></span>
						<span class="toggle-item"></span>
						<span class="toggle-item"></span>
						<span class="toggle-item"></span>
					</div>
				</div>
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
				</nav><!-- #site-navigation -->

				<?php get_template_part('template-parts/woocommerce/shop-related-menu'); ?>

				<?php //get_template_part('template-parts/social-links'); 
				?>
			</div>
		</header><!-- #masthead -->

		<?php echo do_shortcode('[display-categories-menu]');
		?>

		<div class="page-container">
			<div id="content" class="site-content clearfix">
				<div class="wrapper">