const isProduction = process.env.NODE_ENV === "production";
const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ImageminPlugin = require("imagemin-webpack-plugin").default;
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = {
  devtool: "source-map",
  entry: {
    bundle: ["./assets/app"],
    "editor-style": "./assets/sass/editor-style.scss",
    cart: "./assets/sass/woocommerce/cart.scss",
    account: "./assets/sass/woocommerce/account.scss",
    checkout: "./assets/sass/woocommerce/checkout.scss",
    "single-product": "./assets/sass/woocommerce/single-product.scss",
    archive: "./assets/sass/woocommerce/archive-product.scss"
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "js/[name].js"
  },
  externals: {
    jquery: "jQuery"
  },
  module: {
    rules: [
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 10000,
              name: "[name].[ext]",
              publicPath: "../images"
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 10000,
              name: "[name].[ext]",
              outputPath: "fonts/",
              publicPath: "../fonts"
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["env"]
          }
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            },
            { loader: "postcss-loader" }
          ]
        })
      },
      {
        test: /\.scss|sass$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "postcss-loader"
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        })
      }
    ]
  },
  plugins: [
    new FriendlyErrorsWebpackPlugin(),
    new ExtractTextPlugin({
      filename: "css/[name].css",
      disable: false
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: isProduction
    }),
    new CopyWebpackPlugin([{ from: "./assets/images", to: "images" }])
  ]
};

if (isProduction) {
  module.exports.plugins.push(
    new CleanWebpackPlugin("dist"),
    new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i })
  );
}
