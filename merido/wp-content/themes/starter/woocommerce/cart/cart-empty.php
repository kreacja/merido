<?php

/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined('ABSPATH') || exit;

/*
 * @hooked wc_empty_cart_message - 10
 */
do_action('woocommerce_cart_is_empty');
?>

<div class="empty-cart-container">

    <figure>
        <svg xmlns="http://www.w3.org/2000/svg" width="168" height="145.31" viewBox="0 0 168 145.31">
            <g id="empty-cart" transform="translate(0 -11.345)">
                <circle id="Ellipse_567" data-name="Ellipse 567" cx="2" cy="2" r="2" transform="translate(0 138.655)" fill="#2d4356" />
                <path id="Path_271" data-name="Path 271" d="M11,138.655H8a2,2,0,0,0,0,4h3a2,2,0,0,0,0-4Z" fill="#2d4356" />
                <path id="Path_272" data-name="Path 272" d="M160,138.655h-3a2,2,0,0,0,0,4h3a2,2,0,0,0,0-4Z" fill="#2d4356" />
                <circle id="Ellipse_568" data-name="Ellipse 568" cx="2" cy="2" r="2" transform="translate(164 138.655)" fill="#2d4356" />
                <path id="Path_273" class="main-color" data-name="Path 273" d="M118.154,146.655h-8.308a2.006,2.006,0,0,0,0,4h8.308a2.006,2.006,0,0,0,0-4Z"/>
                <path id="Path_274" class="main-color" data-name="Path 274" d="M58.154,146.655H49.846a2.006,2.006,0,0,0,0,4h8.308a2.006,2.006,0,0,0,0-4Z"/>
                <path id="Path_275" class="main-color" data-name="Path 275" d="M104,146.655H64a2,2,0,0,0,0,4H79.94v2H72a2,2,0,0,0,0,4H97a2,2,0,0,0,0-4H88.06v-2H104a2,2,0,0,0,0-4Z"/>
                <path id="Path_276" class="main-color" data-name="Path 276" d="M144.06,116.035l-28.71,7.09.6-4.27,27.07-6.68Z"/>
                <path id="Path_277" class="main-color" data-name="Path 277" d="M109.66,55.515l-3.9.97a11.31,11.31,0,0,0-2.92-4.29,19.4,19.4,0,0,0-10.41-6.79,15.429,15.429,0,0,1,17.23,10.11Z"/>
                <path id="Path_278" data-name="Path 278" d="M147.1,119.685a8.253,8.253,0,0,1-5.3,4.12l-27.49,6.79.59-4.27,25.94-6.4a4.245,4.245,0,0,0,3.22-3.89l-1.04-3.86-18.79-52.24a4.222,4.222,0,0,0-3.98-2.78,4.439,4.439,0,0,0-1.02.12l-12.6,3.11c-.27-1.3-.87-3.9-.87-3.9l3.9-.97,8.61-2.12a8.252,8.252,0,0,1,9.73,5.19l19.59,54.46a8.191,8.191,0,0,1-.49,6.64Z" fill="#2d4356" />
                <path id="Path_279" data-name="Path 279" d="M150.72,138.655h-42.5a8.121,8.121,0,0,0,1.42-3.57l8.01-57.33a8.2,8.2,0,0,0-6.19-9.12l-8.61-2.12-3.9-.97-5.14-1.27-5.02-1.24-3.9-.96-22.65-5.59-3.91-.97-8.6-2.12A8.252,8.252,0,0,0,40,58.585l-19.59,54.46A8.232,8.232,0,0,0,26.2,123.8s38.75,9.53,60.66,14.85c-19.838-.534-69.58,0-69.58,0s-1.561.112-1.964.916a2.242,2.242,0,0,0,0,2.167c.34.669,1.964.916,1.964.916H150.72a2.017,2.017,0,1,0,0-4Zm-45.59-2.57a4.245,4.245,0,0,1-3.63,2.07,4.041,4.041,0,0,1-1.03-.13l-73.31-18.1a4.245,4.245,0,0,1-3.22-3.89l1.04-3.86,18.79-52.24a4.222,4.222,0,0,1,3.98-2.78c.34,0,62.75,15.36,62.75,15.36a4.244,4.244,0,0,1,3.19,4.69l-7.68,54.98Z" fill="#2d4356" />
                <path id="Path_280" class="main-color" data-name="Path 280" d="M88.79,63.035l-3.9-.96a11.442,11.442,0,0,0-8.64-12.43l-.58-.15a11.463,11.463,0,0,0-13.43,6.99l-3.91-.97a15.474,15.474,0,0,1,18.3-9.9l.58.14a15.447,15.447,0,0,1,11.58,17.28Z"/>
                <path id="Path_281" class="main-color" data-name="Path 281" d="M102.85,66.515l-3.9-.97A11.476,11.476,0,0,0,91.54,53.5a19.527,19.527,0,0,0-1.38-2.79,20.541,20.541,0,0,0-1.36-1.95,17.8,17.8,0,0,1,1.89.33l.58.15A15.465,15.465,0,0,1,102.85,66.515Z"/>
                <path id="Path_282" class="main-color" data-name="Path 282" d="M106.01,132.185l-.88,3.9-81.19-20.05,1.04-3.86Z"/>
                <path id="Path_283" data-name="Path 283" d="M126,44.655a3,3,0,1,0-3-3,3,3,0,0,0,3,3Zm0-4.5a1.5,1.5,0,1,1-1.5,1.5,1.5,1.5,0,0,1,1.5-1.5Z" fill="#2d4356" />
                <path id="Path_284" data-name="Path 284" d="M155,48.829a2,2,0,1,0,2,2,2,2,0,0,0-2-2Zm0,3a1,1,0,1,1,1-1A1,1,0,0,1,155,51.829Z" fill="#2d4356" />
                <path id="Path_285" data-name="Path 285" d="M50,35.655a2,2,0,1,0,2,2A2,2,0,0,0,50,35.655Zm0,3a1,1,0,1,1,1-1,1,1,0,0,1-1,1Z" fill="#2d4356" />
                <path id="Path_286" data-name="Path 286" d="M147,27.655a2,2,0,1,0,2,2,2,2,0,0,0-2-2Zm0,3a1,1,0,1,1,1-1,1,1,0,0,1-1,1Z" fill="#2d4356" />
                <path id="Path_287" data-name="Path 287" d="M17.3,25.655a2,2,0,1,0-2,2,2,2,0,0,0,2-2Zm-3,0a1,1,0,1,1,1,1,1,1,0,0,1-1-1Z" fill="#2d4356" />
                <path id="Path_288" class="main-color" data-name="Path 288" d="M10.888,66.667l1.487-1.956-.939-.532-.955,2.19H10.45l-.97-2.174-.955.547L10,66.651v.032l-2.3-.3v1.064l2.316-.3v.031L8.525,69.092l.891.563,1.018-2.206h.031l.939,2.19.986-.563-1.5-1.877v-.032l2.362.282V66.385l-2.362.313Z"/>
                <path id="Path_289" class="main-color" data-name="Path 289" d="M70.169,15.29l-.856,1.1.514.324.586-1.27h.017l.541,1.261.568-.324L70.674,15.3v-.017l1.36.162V14.83l-1.36.18v-.018l.856-1.126-.541-.306-.549,1.261h-.018l-.559-1.252-.55.315.847,1.1V15l-1.325-.171v.613l1.334-.171Z"/>
                <path id="Path_290" class="main-color" data-name="Path 290" d="M164.724,14.552V13.508l-2.317.307v-.031l1.459-1.918-.921-.521-.936,2.148h-.032l-.95-2.133-.938.537,1.444,1.872V13.8l-2.257-.292v1.044l2.272-.291v.03l-1.459,1.872.875.553,1-2.164h.029l.921,2.148.968-.552-1.474-1.842v-.03Z"/>
                <path id="Path_291" class="main-color" data-name="Path 291" d="M108.351,17.241l1.258-1.654-.794-.45-.807,1.852h-.027l-.821-1.84-.808.464,1.245,1.615v.027L105.65,17v.9l1.96-.251v.026l-1.258,1.615.754.477.862-1.867h.026l.794,1.853.834-.476-1.271-1.589v-.026l2,.238V17l-2,.265Z"/>
            </g>
        </svg>
    </figure>

    <h2><?= __('Twój koszyk jest pusty', 'starter'); ?></h2>
    <p><?= __('Może zainteresują Cię te kategorie:', 'starter'); ?></p>

    <?php
    $number_of_items_to_display = 3;
    $categories = get_categories([
        'taxonomy' => 'product_cat',
        'number' => $number_of_items_to_display
    ]);

    if (isset($categories) && !empty($categories)) : ?>
        <div class="categories-suggested">
            <?php foreach ($categories as $index => $category) : ?>
                <a href="<?php echo get_term_link($category) ?>"><?php echo $category->name; ?></a>
                <?php echo $index < ($number_of_items_to_display - 1) ? " | " : ""; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

<?php if (wc_get_page_id('shop') > 0) : ?>
    <p class="return-to-shop">
        <a class="button wc-backward" href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
            <?php
            /**
             * Filter "Return To Shop" text.
             *
             * @since 4.6.0
             * @param string $default_text Default text.
             */
            echo esc_html(apply_filters('woocommerce_return_to_shop_text', __('Return to shop', 'woocommerce')));
            ?>
        </a>
    </p>
<?php endif; ?>