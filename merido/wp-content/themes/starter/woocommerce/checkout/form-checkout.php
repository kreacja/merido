<?php

/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_checkout_form', $checkout);

echo load_template_part('template-parts/woocommerce/cart-checkout-order-nav', null, ['section' => 'checkout']);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
    echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
    return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

    <div class="till">
        <h3 class="heading"><?= __('Twoje dane', 'starter'); ?></h3>
        <?php if ($checkout->get_checkout_fields()) : ?>

            <?php do_action('woocommerce_checkout_before_customer_details'); ?>

            <div class="col2-set" id="customer_details">

                <div class="col-1">
                    <?php do_action('woocommerce_checkout_billing'); ?>
                </div>

                <div class="col-2">
                    <?php do_action('woocommerce_checkout_shipping'); ?>
                </div>
            </div>

            <?php do_action('woocommerce_checkout_after_customer_details'); ?>

        <?php endif; ?>
    </div>

    <div>

        <div class="till shipping-methods">
            <?php if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) : ?>

                <h3 class="heading"><?= __('Sposób dostawy', 'starter'); ?></h3>

                <div class="inner">
                    <?php do_action('woocommerce_review_order_before_shipping'); ?>

                    <?php wc_cart_totals_shipping_html(); ?>

                    <?php do_action('woocommerce_review_order_after_shipping'); ?>
                </div>

            <?php endif; ?>
        </div>

        <div class="till">
            <h3 class="heading"><?= __('Metody płatności', 'starter'); ?></h3>
            <?php if (!is_ajax()) {
                do_action('woocommerce_review_order_before_payment');
            }
            ?>
            <div id="payment" class="woocommerce-checkout-payment">
                <?php if (WC()->cart->needs_payment()) : ?>
                    <ul class="wc_payment_methods payment_methods methods">
                        <?php
                        if (!empty($available_gateways)) {
                            foreach ($available_gateways as $gateway) {
                                wc_get_template('checkout/payment-method.php', array('gateway' => $gateway));
                            }
                        } else {
                            echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters('woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__('Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce') : esc_html__('Please fill in your details above to see available payment methods.', 'woocommerce')) . '</li>'; // @codingStandardsIgnoreLine
                        }
                        ?>
                    </ul>
                <?php endif; ?>
            </div>
            <?php
            if (!is_ajax()) {
                do_action('woocommerce_review_order_after_payment');
            } ?>

            <?php do_action('woocommerce_checkout_before_order_review_heading'); ?>
        </div>

    </div>

    <?php do_action('woocommerce_checkout_before_order_review'); ?>

    <div class="till review">
        <h3 class="heading"><?= __('Podsumowanie', 'starter'); ?></h3>
        <div id="order_review" class="woocommerce-checkout-review-order">
            <?php do_action('woocommerce_checkout_order_review'); ?>
        </div>

        <?php do_action('woocommerce_review_order_before_submit'); ?>

        <?php wc_get_template('checkout/terms.php'); ?>

        <div class="button-wrapper">
            <?php
            $order_button_text = __('Zamawiam i płacę', 'kalithea');
            echo apply_filters('woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr($order_button_text) . '" data-value="' . esc_attr($order_button_text) . '">' . esc_html($order_button_text) . '</button>'); // @codingStandardsIgnoreLine
            ?>
        </div>

        <?php do_action('woocommerce_review_order_after_submit'); ?>

    </div>

    <?php do_action(' woocommerce_checkout_after_order_review '); ?>

</form>

<?php do_action(' woocommerce_after_checkout_form', $checkout); ?>