<?php

/**
 * Checkout terms and conditions area.
 *
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

if (apply_filters('woocommerce_checkout_show_terms', true) && function_exists('wc_terms_and_conditions_checkbox_enabled')) {
    do_action('woocommerce_checkout_before_terms_and_conditions');

?>
    <div class="woocommerce-terms-and-conditions-wrapper">
        <label for="privacy">
            <input type="checkbox" name="privacy" id="privacy">
            <span><?= __('Zapoznałem/am się z <a href="' . home_url() . '/warunki-zakupow/polityka-prywatnosci-i-cookies/" target="_blank">polityką prywatności</a>.', 'starter'); ?><span class="is-required">*</span></span>
        </label>
        <label for="terms">
            <input type="checkbox" name="terms" id="terms">
            <span><?= __('Przeczytałem/am i akceptuję <a href="' . home_url() . '/warunki-zakupow/regulamin/" target="_blank">regulamin</a>.', 'starter'); ?><span class="is-required">*</span></span>
        </label>
        <label for="marketing">
            <input type="checkbox" name="marketing" id="marketing">
            <span><?= __('Wyrażam zgodę na otrzymywanie wiadomości marketingowych i informacji o ofertach na mój e-mail.', 'starter'); ?></span>
        </label>
    </div>
<?php

    do_action('woocommerce_checkout_after_terms_and_conditions');
}
