<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class('', $product); ?>>

    <?php
    /**
     * Hook: woocommerce_before_single_product_summary.
     *
     * @hooked woocommerce_show_product_sale_flash - 10
     * @hooked woocommerce_show_product_images - 20
     */
    remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
    do_action('woocommerce_before_single_product_summary');
    ?>

    <div class="summary entry-summary">
        <?php

        woocommerce_template_single_rating(); ?>

        <div class="title-add-to-fav-share">
            <h1><?php the_title(); ?></h1>
            <?php get_template_part('/template-parts/woocommerce/single-product/add-to-fav-share'); ?>
        </div>

        <?php if ($product->get_type() === "simple") : ?>
            <?php $sku = $product->get_sku(); ?>
            <?php if ($sku) : ?>
                <div class="sku-container">
                    <?= __('SKU:', 'starter') . " " . $sku; ?>
                </div>
            <?php endif; ?>

            <div class="price-container">
                <?php woocommerce_show_product_sale_flash(); ?>
                <?php woocommerce_template_single_price(); ?>
            </div>
        <?php else : ?>
            <div class="price-container">
                <?php woocommerce_template_single_price(); ?>
            </div>
        <?php endif; ?>

        <div class="brand-container">
            <?php echo do_shortcode('[pwb-brand as_link="true"]'); ?>
        </div>

        <div class="short-description">
            <?php echo $product->get_short_description(); ?>
        </div>

        <?php woocommerce_template_single_add_to_cart(); ?>

        <?php
        $delivery_time_settings = get_field('delivery-time-settings', 'options');
        $delivery = $delivery_time_settings['delivery-time-default'];

        if (get_field('product-delivery-time')) {
            $delivery = get_field('product-delivery-time');
        }

        if ($delivery_time_settings['display-delivery-time'] && $delivery) :
        ?>
            <div class="delivery">
                <svg id="delivery" xmlns="http://www.w3.org/2000/svg" width="16" height="19" viewBox="0 0 16 19">
                    <g id="Group_141" data-name="Group 141">
                        <g id="Group_139" data-name="Group 139" transform="translate(0 0)">
                            <g id="Group_134" data-name="Group 134">
                                <path id="Path_249" data-name="Path 249" d="M50.7,49.389l-8-4.75v-9.5l8-4.75,8,4.75v9.5Zm-7.058-5.31L50.7,48.27l7.058-4.191V35.7L50.7,31.508,43.641,35.7Z" transform="translate(-42.699 -30.389)" />
                            </g>
                            <g id="Group_135" data-name="Group 135" transform="translate(4.648 7.249)">
                                <rect id="Rectangle_255" data-name="Rectangle 255" width="3.672" height="0.978" transform="matrix(0.866, 0.5, -0.5, 0.866, 0.489, 0)" />
                            </g>
                            <g id="Group_136" data-name="Group 136" transform="translate(0.244 4.604)">
                                <rect id="Rectangle_256" data-name="Rectangle 256" width="2.918" height="0.978" transform="matrix(0.866, 0.5, -0.5, 0.866, 0.489, 0)" />
                            </g>
                            <g id="Group_137" data-name="Group 137" transform="translate(7.458 4.594)">
                                <rect id="Rectangle_257" data-name="Rectangle 257" width="0.978" height="9.017" transform="matrix(0.5, 0.866, -0.866, 0.5, 7.809, 0)" />
                            </g>
                            <g id="Group_138" data-name="Group 138" transform="translate(7.511 9.145)">
                                <rect id="Rectangle_258" data-name="Rectangle 258" width="0.978" height="9.291" transform="translate(0 0)" />
                            </g>
                        </g>
                        <g id="Group_140" data-name="Group 140" transform="translate(2.285 1.682)">
                            <path id="Path_250" data-name="Path 250" d="M65.18,52.18l-2.828-1.5v-2.56l8.053-4.65.489.847L63.33,48.682V50.09l.873.464V49.1l8.053-4.649.489.846L65.18,49.665Z" transform="translate(-62.352 -43.468)" />
                        </g>
                    </g>
                </svg>
                <?= __('Czas dostawy:', 'starter'); ?> <?php echo $delivery . " dni"; ?>
            </div>
        <?php endif; ?>

        <div class="category">
            <p><?= __('Kategoria:', 'starter'); ?> <?php echo wc_get_product_category_list(get_the_ID()); ?></p>
        </div>
    </div>

    <?php
    /**
     * Hook: woocommerce_after_single_product_summary.
     *
     * @hooked woocommerce_output_product_data_tabs - 10
     * @hooked woocommerce_upsell_display - 15
     * @hooked woocommerce_output_related_products - 20
     */
    do_action('woocommerce_after_single_product_summary');
    ?>
</div>

<?php do_action('woocommerce_after_single_product'); ?>