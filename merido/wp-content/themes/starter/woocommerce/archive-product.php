<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

get_header('shop');

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
do_action('woocommerce_before_main_content');
?>

<?php if (!is_front_page()) : ?>
    <?php
    $qo = get_queried_object();
    $bg = "()";
    if (isset($qo) && $qo instanceof WP_Term) {
        $bg = get_field('header-bg', $qo);
        $font_color = get_field('header-font-color', $qo);
    }
    ?>
    <div class="header-wrapper" style="background-image: url(<?php echo $bg ?>); color: <?php echo $font_color; ?> !important">;
        <div class="wrapper">
            <?php if (isset($font_color)) : ?>
                <style>
                    nav.woocommerce-breadcrumb,
                    .woocommerce-breadcrumb a {
                        color: <?php echo $font_color; ?> !important;
                    }
                </style>
            <?php endif; ?>
            <?php woocommerce_breadcrumb(); ?>

            <header class="woocommerce-products-header">
                <?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
                    <h1 class="woocommerce-products-header__title page-title" style="color: <?php echo $font_color; ?> !important"><?php woocommerce_page_title(); ?></h1>
                <?php endif; ?>

                <?php
                /**
                 * Hook: woocommerce_archive_description.
                 *
                 * @hooked woocommerce_taxonomy_archive_description - 10
                 * @hooked woocommerce_product_archive_description - 10
                 */
                do_action('woocommerce_archive_description');
                ?>
            </header>
        </div>
    </div>
<?php endif; ?>

<?php if (!is_front_page()) : ?>
    <?php
    if (isset($qo) && $qo instanceof WP_Term) {
        $display_sidebar = get_field('display-sidebar', $qo);

        if($display_sidebar) {
            echo do_shortcode('[display-archive-with-sidebar]');
        } else {
            echo do_shortcode('[display-archive-without-sidebar]');
        }
    }
    ?>
<?php else : ?>
    <?php echo do_shortcode('[display-categories-tiles]'); ?>
    
    <?php echo do_shortcode('[products limit=8]'); ?>
    <?php // echo do_shortcode('[display-recommended-products]');  ?>

    <div class="shop-page-content">
        <?php echo get_the_content(null, null, wc_get_page_id('shop')); ?>
    </div>

<?php endif; ?>

<?php if (isset($qo) && $qo instanceof WP_Term) {
    $additional_description = get_field('additional-description', $qo);
}

if (isset($additional_description)) : ?>
    <div class="additional-description-wrapper">
        <?php echo $additional_description; ?>
    </div>
<?php endif; ?>

<?php echo do_shortcode('[display-newsletter-form]'); ?>

<?php
/**
* Hook: woocommerce_after_main_content.
*
* @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
*/
do_action('woocommerce_after_main_content');

/**
* Hook: woocommerce_sidebar.
*
* @hooked woocommerce_get_sidebar - 10
*/
// do_action('woocommerce_sidebar');

get_footer('shop');