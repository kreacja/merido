# Lista shortcode

### Wyświetlanie prefooter (nad właściwym footerem)
Szablon gotowy do edycji w panelu (Szablony -> Zapisane szablony -> Prefooter)
```php
[elementor-template id="406"]
```

### Wyświetlanie menu kategorii pod głównym menu (header.php)
Wyświetla listę kategorii (w wersji mobilnej wyświetla pasek, który można przesuwać suwakiem/dotykiem lewo-prawo)
```php
[display-categories-menu]
```

### Wyświetlanie kafelków kategorii (archive.php)
Wyświetla kafle z kategoriami
```php
[display-categories-tiles]
```

### Wyświetlanie listy produtków z sidebarem (lub bez) (archive-product.php)
Jeżeli lista produktów ma być wyświetlana z sidebarem, należy użyć kodu:
```php
[display-archive-with-sidebar]
```
W przeciwnym razie:
```php
[display-archive-without-sidebar]
```

### Wyświetlanie polecanych produktów (archive-product.php)
```php
[display-recommended-products]
```

### Przyciski przy kaflu produktu (content-product.php)
Aby wyświetlić tylko przycisk "Dodaj do koszyka":
```php
[simple-add-to-cart]
```
Aby wyświetlić przyciski "Dodaj do koszyka", "Dodaj do ulubionych" i "Szczegóły":
```php
[extended-add-to-cart]
```

### Wyświetlanie formularza newsletter (archive-product.php)
```php
[display-newsletter-form]
```