<?php 
get_header(); 
the_post();
?>

	<div id="primary" class="full content-area">
		<main id="main" class="site-main" role="main">

			<header class="page-header">
				<h1 class="page-title section-title"><?php the_title(); ?></h1>
				<?php the_post_thumbnail( 'large' ) ?>
			</header><!-- .entry-header -->

			<?php get_template_part( 'content-single', get_post_format() ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
