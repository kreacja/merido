<?php

require get_template_directory() . '/inc/helpers.php';
require get_template_directory() . '/inc/kreacja.php';
require get_template_directory() . '/inc/Api/Products.php';
require get_template_directory() . '/inc/Api/Categories.php';

class KreacjaApp
{
	function __construct()
	{
		add_action('after_setup_theme', [$this, 'themeSetup']);
		add_action('widgets_init', [$this, 'themeWidgetsInit']);
		add_action('wp_enqueue_scripts', [$this, 'themeScripts']);
	}

	public function themeSetup()
	{
		$font_url = str_replace(',', '%2C', '//fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
	    add_editor_style($font_url);
	    add_editor_style('dist/css/editor-style.css');

		load_theme_textdomain('nazwa', get_template_directory() . '/languages');

		add_theme_support('automatic-feed-links');

		add_theme_support('title-tag');

		add_theme_support('post-thumbnails');

		register_nav_menus(
			[
				'primary' => __('Menu główne', 'nazwa'),
				'footer' => __('Menu w stopce','nazwa')
			]
		);

		add_theme_support('html5', ['search-form', 'comment-form', 'comment-list', 'gallery', 'caption']);

		add_theme_support('woocommerce');
	}

	public function themeWidgetsInit()
	{
		register_sidebar([
			'name'          => __('Panel główny', 'nazwa'),
			'id'            => 'sidebar-main',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		]);
	}

	public function themeScripts()
	{
		$ver = defined('WP_DEBUG') && true === WP_DEBUG ? '?'.time() : null;

		wp_enqueue_style('nazwa-main', get_template_directory_uri() . '/dist/css/bundle.css'.$ver, [], $ver);
		wp_enqueue_style('nazwa', get_stylesheet_uri());

		if (is_cart())
			wp_enqueue_style('wc-cart-css', get_template_directory_uri() . '/dist/css/cart.css' . $ver, [], $ver);

		if (is_account_page())
			wp_enqueue_style('wc-account-css', get_template_directory_uri() . '/dist/css/account.css' . $ver, [], $ver);

		if (is_product())
			wp_enqueue_style('wc-single-product-css', get_template_directory_uri() . '/dist/css/single-product.css' . $ver, [], $ver);

		if (is_product_category() || is_front_page() || is_product())
			wp_enqueue_style('wc-archive-css', get_template_directory_uri() . '/dist/css/archive.css' . $ver, [], $ver);

		if (is_checkout())
			wp_enqueue_style('wc-checkout-css', get_template_directory_uri() . '/dist/css/checkout.css' . $ver, [], $ver);

		## Remove Gutenberg styles
		wp_dequeue_style('wp-block-library');
		wp_dequeue_style('wp-block-library-theme');
		wp_dequeue_style('wc-block-style');

		## Dequeue WooCommerce styles
		// wp_dequeue_style('woocommerce-layout');
		// wp_dequeue_style('woocommerce-general');
		// wp_dequeue_style('woocommerce-smallscreen');

		## Dequeue WooCommerce scripts
		// wp_dequeue_script('wc-cart-fragments');
		// wp_dequeue_script('woocommerce');
		// wp_dequeue_script('wc-add-to-cart');

		// wp_deregister_style('google-custom-fonts');
		// wp_enqueue_style('google-custom-fonts', '//fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap', array(), '1.1', 'all');

		wp_enqueue_script('bundle-js', get_template_directory_uri() . '/dist/js/bundle.js'.$ver, array('jquery'), $ver, true);
		
		wp_localize_script( 'bundle-js', 'jsData', ['ajaxUrl' => admin_url('admin-ajax.php')] );

		if (is_singular() && comments_open() && get_option('thread_comments'))
			wp_enqueue_script( 'comment-reply' );
	}

}

$kreacjaApp = new KreacjaApp();