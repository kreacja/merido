<?php get_header(); ?>

	<div id="primary" class="content-area full">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Ups! Strona nie istnieje.', 'nazwa' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'Niestety nie znaleziono takiej strony. Użyj menu aby odnaleźć to czego potrzebujesz.', 'nazwa' ); ?></p>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>