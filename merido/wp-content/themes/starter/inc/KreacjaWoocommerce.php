<?php

class KreacjaWoocommerce
{
    public function __construct()
    {
        $this->registerActions();
        $this->registerFilters();
    }

    protected function registerActions()
    {
        add_action('woocommerce_checkout_process', [$this, 'sk_custom_checkout_field_process']);
        add_action( 'woocommerce_admin_order_item_headers', [ $this, 'woocommerce_tax_brutto_headers' ] );
        add_action( 'woocommerce_admin_order_item_values', [ $this , 'woocommerce_tax_brutto_values' ], 10, 3);
    }

    protected function registerFilters()
    {
        add_filter('loop_shop_columns', [$this, 'sk_loop_columns'], 999);
        add_filter('woocommerce_add_to_cart_fragments', [$this, 'sk_add_to_cart_fragment']);
        add_filter('woocommerce_checkout_fields', [$this, 'sk_checkout_fields']);
        add_filter('woocommerce_rest_prepare_product_object', [$this, 'sk_prepare_product_images_for_searchform'], 10, 3);
        add_filter('woocommerce_get_stock_html', [$this, 'sk_change_stock_info'], 10, 2);
        add_filter('woocommerce_available_variation', [$this, 'sk_display_variation_sku_and_gtin'], 20, 3);
    }

    public function sk_custom_checkout_field_process()
    {
        if (!$_POST['privacy']) {
            wc_add_notice(__('Należy zapoznać się z polityką prywatności.'), 'error');
        }
        if (!$_POST['terms']) {
            wc_add_notice(__('Należy zaakceptować regulamin.'), 'error');
        }
    }

    public function sk_loop_columns ()
    {
        return get_field('products-in-a-row', 'options');
    }

    public function sk_add_to_cart_fragment ($fragments)
    {
        ob_start();
        get_template_part("template-parts/woocommerce/shop-related-menu-cart");

        $fragments['.cart-icon-wrapper'] = ob_get_clean();

        return $fragments;
    }

    public function sk_checkout_fields($fields)
    {
        $fields['billing']['billing_first_name']['placeholder'] = __('Imię', 'starter');
        $fields['billing']['billing_first_name']['label'] = "";
        $fields['billing']['billing_first_name']['class'][0] = "form-row-wide";
        $fields['billing']['billing_last_name']['label'] = "";
        $fields['billing']['billing_last_name']['placeholder'] = __('Nazwisko', 'starter');
        $fields['billing']['billing_last_name']['class'][0] = "form-row-wide";
        // unset($fields['billing']['billing_company']);
        $fields['billing']['billing_country']['label'] = "";
        $fields['billing']['billing_address_1']['label'] = "";
        $fields['billing']['billing_postcode']['label'] = "";
        $fields['billing']['billing_postcode']['placeholder'] = __('Kod pocztowy', 'starter');
        $fields['billing']['billing_postcode']['class'][1] = "form-row-first";
        $fields['billing']['billing_city']['label'] = "";
        $fields['billing']['billing_city']['placeholder'] = __('Miasto', 'starter');
        $fields['billing']['billing_city']['class'][0] = "form-row-last";
        $fields['billing']['billing_phone']['label'] = "";
        $fields['billing']['billing_phone']['placeholder'] = __('Telefon', 'starter');
        $fields['billing']['billing_email']['label'] = "";
        $fields['billing']['billing_email']['placeholder'] = __('E-mail', 'starter');
        $fields['billing']['billing_postcode']['class'][0] = "form-row-first";
        $fields['billing']['billing_city']['class'][0] = "form-row-last";
        unset($fields['billing']['billing_address_2']);
        unset($fields['billing']['billing_state']);

        $fields['shipping']['shipping_first_name']['class'][0] = "form-row-wide";
        $fields['shipping']['shipping_first_name']['label'] = "";
        $fields['shipping']['shipping_first_name']['placeholder'] = __('Imię', 'starter');
        $fields['shipping']['shipping_last_name']['class'][0] = "form-row-wide";
        $fields['shipping']['shipping_last_name']['label'] = "";
        $fields['shipping']['shipping_last_name']['placeholder'] = __('Nazwisko', 'starter');
        unset($fields['shipping']['shipping_company']);
        $fields['shipping']['shipping_postcode']['class'][1] = "form-row-first";
        $fields['shipping']['shipping_country']['label'] = "";
        $fields['shipping']['shipping_address_1']['label'] = "";
        $fields['shipping']['shipping_postcode']['label'] = "";
        $fields['shipping']['shipping_postcode']['placeholder'] = __('Kod pocztowy', 'starter');
        $fields['shipping']['shipping_city']['class'][0] = "form-row-last";
        $fields['shipping']['shipping_city']['label'] = "";
        $fields['shipping']['shipping_city']['placeholder'] = __('Miasto', 'starter');
        // unset($fields['shipping']['shipping_address_2']);

        $fields['order']['order_comments']['label'] = "";
        $fields['order']['order_comments']['placeholder'] = __('Uwagi do zamówienia', 'starter');

        // echo '<pre>';
        // print_r($fields);
        // echo '</pre>';

        return $fields;
    }

    public function sk_prepare_product_images_for_searchform ($response, $post, $request)
    {
        global $_wp_additional_image_sizes;

        if (empty($response->data)) {
            return $response;
        }

        foreach ($response->data['images'] as $key => $image) {
            $image_urls = [];
            foreach ($_wp_additional_image_sizes as $size => $value) {
                $image_info = wp_get_attachment_image_src($image['id'], $size);
                $response->data['images'][$key][$size] = $image_info[0];
            }
        }
        return $response;
    }

    public function sk_change_stock_info ($html, $product)
    {
        if ($product->is_in_stock()) {
            $qty = $product->get_stock_quantity();

            if (!isset($qty)) {
                $html = "<p class='stock in-stock'>" . __('Na stanie', 'starter') . "</p>";
            }
        } 

        return $html;
    }

    public function sk_display_variation_sku_and_gtin($variation_data, $product, $variation)
    {
        $html = ''; // Initializing

        // Inserting SKU
        if (!empty($variation_data['sku'])) {
            $html .= '</div><div class="woocommerce-variation-sku">' . __('SKU:') . ' ' . $variation_data['sku'];
        }

        // Using the variation description to add dynamically the SKU and the GTIN
        $variation_data['variation_description'] .= $html;

        return $variation_data;
    }
    
    public function woocommerce_tax_brutto_headers()
    {
        echo '<th>'.__( 'Cena Brutto', 'starter' ).'</th>';
    }
    public function woocommerce_tax_brutto_values( $null, $item, $item_id )
    {
        $price =  $item->get_total() ;
        $tax = $item->get_total_tax();
        $total = wc_price( floatval( $price) + floatval($tax) );
        echo "<td>{$total}</td>";
    }

}