<?php 

class KreacjaRodo
{
    public function __construct()
	{
		$this->registerShortcodes();
    }

    public function registerShortcodes() {
        add_shortcode( 'rodo_kind_of_page', [$this, 'rodo_kind_of_page'] );
        add_shortcode( 'rodo_page_name', [$this, 'rodo_page_name'] );
        add_shortcode( 'rodo_company_datas', [$this, 'rodo_company_datas'] );
        add_shortcode( 'rodo_social_login', [$this, 'rodo_social_login'] );
        add_shortcode( 'rodo_newsletter', [$this, 'rodo_newsletter'] );
        add_shortcode( 'rodo_other_portals_cookies', [$this, 'rodo_other_portals_cookies'] );
        add_shortcode( 'rodo_inspector_contact_datas', [$this, 'rodo_inspector_contact_datas'] );
        add_shortcode( 'rodo_personal_data_inspector', [$this, 'rodo_personal_data_inspector'] );
        add_shortcode( 'rodo_published_date', [$this, 'rodo_published_date'] );
    }

    public function rodo_kind_of_page($atts) {
        $kind_of_page_group = get_field('rodo_kind_of_page_group');
        $kind_of_page = $kind_of_page_group['kind_of_page'];

        return $kind_of_page == 'standard' ? $kind_of_page_group['standard_page'] : $kind_of_page_group['shop'];
    }

    public function rodo_page_name($atts) {
        $site_url = get_site_url();

        return parse_url($site_url)['host'];
    }
    
    public function rodo_company_datas($atts) {
        $company_datas = get_field('rodo_company_datas');

        if($company_datas) {
            $company_datas_implode = implode(', ', array_filter($company_datas));
            return $company_datas_implode;
        }
    }
    
    public function rodo_social_login($atts) {
        $social_login_group = get_field('rodo_social_login_group');

        if($social_login_group) {
            $content = '';
        
            if($social_login_group['rodo_social_login_choice']) {
                $content .= '<h3>'. __('LOGOWANIE FB / GOOGLE', 'nazwa') .'</h3>';
                $content .= '<p>'.$social_login_group['rodo_social_login_description'].'</p>';
                return $content;
            }
        }
    }
    
    public function rodo_newsletter($atts) {
        $newsletter_group = get_field('rodo_newsletter_group');	
        if($newsletter_group) {
            $content = '';
    
            if($newsletter_group['rodo_newsletter_choice']) {
                $content .= '<h3>'. __('NEWSLETTER', 'nazwa') .'</h3>';
                $content .= '<p>'.$newsletter_group['rodo_newsletter_description'].'</p>';
                return $content;
            }
        }
    }
    
    public function rodo_other_portals_cookies($atts) {
        $other_portals_cookies = get_field('rodo_other_portals_cookies');
    
        if($other_portals_cookies)	{
            $other_portals_cookies_implode = implode(', ', $other_portals_cookies);
            return $other_portals_cookies_implode;
        }
    }
    
    public function rodo_personal_data_inspector($atts) {
        $personal_data_inspector = get_field('rodo_personal_data_inspector');	
        
        if($personal_data_inspector) {
            return $personal_data_inspector;
        }
    }
    
    public function rodo_inspector_contact_datas($atts) {
        $contact_datas = get_field('rodo_inspector_contact_datas_group');	

        if($contact_datas) {
            $company_datas_field_obj_fields = get_field_object('rodo_inspector_contact_datas_group')['sub_fields'];
            $content = (string)"";
            $contentList = (string)"";
        
            foreach ($company_datas_field_obj_fields as $key => $datas) {
                $field_value = $contact_datas[$datas['name']];
                if ($field_value) {
                    $field_label = '<strong>'.$datas['label'].'</strong>';
                    $contentList .= '<li>'.$field_label.' '.$field_value.'</li>';
                }
            }
        
            $content = '<ul>'.$contentList.'</ul>';
        
            return $content;
        }
    }
    
    public function rodo_published_date($atts) {
        return get_the_date('d.m.Y');
    }
}

new KreacjaRodo();