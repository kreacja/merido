<?php

function categoriesRoute() {

    register_rest_route('categories/v1', 'search', [
        'methods' => 'GET',
        'callback' => 'getCategoriesBySearch' 
    ] );
    
}

function getCategoriesBySearch( $data ) {

    $terms = get_terms([
        'search' => $data['key'],
        'taxonomy' => 'product_cat'
    ]);

    $data = [];

    foreach ($terms as $term) {
        array_push($data, [
            'name' => $term->name,
            'link' => get_term_link($term)
        ]);
    }
    return $data;

}

add_action( 'rest_api_init', 'categoriesRoute' );
