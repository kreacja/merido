<?php

    function productsRoute() {

        register_rest_route('products/v1', 'search', [
           'methods' => 'GET',
           'callback' => 'getProductsBySearch' 
        ] );
        
    }

    function getProductsBySearch( $data ) {

        $products = wc_get_products(array(
            'status' => 'publish', 
            's' => sanitize_text_field( $data['key'] ),
            'limit' => 12,
        ));

        $data = [];

        foreach ( $products as $product ) {
            $imageUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ) );

            array_push($data, [
                'name' => $product->name,
                'permalink' => get_permalink($product->id),
                'images' => [
                    'shop_thumbnail' => $imageUrl[0]
                ],
                'price' => wc_price( wc_get_price_to_display( $product, array( 'price' => $product->get_regular_price() ) ) )
            ]);
        }
        return $data;

    }

    add_action( 'rest_api_init', 'productsRoute' );
?>