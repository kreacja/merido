<?php

class KreacjaPostTypes
{
	public function __construct()
	{
		//add_action( 'init', [$this, 'registerPostTypes']);
	}

	public function registerPostTypes()
	{
		$this->registerOfferPostType();
	}

	private function registerOfferPostType()
	{
		$labels = array(
			'name'                => __( 'Oferta', 'nazwa' ),
			'singular_name'       => __( 'Oferta', 'nazwa' ),
			'menu_name'           => __( 'Oferta', 'nazwa' ),
			'name_admin_bar'      => __( 'Oferta', 'nazwa' ),
			'parent_item_colon'   => __( 'Parent Item:', 'nazwa' ),
			'all_items'           => __( 'Wszystkie oferty', 'nazwa' ),
			'add_new_item'        => __( 'Dodaj ofertę', 'nazwa' ),
			'add_new'             => __( 'Dodaj nową ofertę', 'nazwa' ),
			'new_item'            => __( 'Oferta', 'nazwa' ),
			'edit_item'           => __( 'Edytuj ofertę', 'nazwa' ),
			'update_item'         => __( 'Zaktualizuj ofertę', 'nazwa' ),
			'view_item'           => __( 'Zobacz ofertę', 'nazwa' ),
			'search_items'        => __( 'Szukaj ofertę', 'nazwa' ),
			'not_found'           => __( 'Brak ofert', 'nazwa' ),
			'not_found_in_trash'  => __( 'Brak ofert w koszu', 'nazwa' ),
		);

		$args = array(
			'description'         => '',
			'labels'              => $labels,
			'supports'            => array( 'title' , 'editor', 'thumbnail', 'revisions' ),
			'taxonomies'          => array( ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 6,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => array( 'slug' => 'oferta' ),
			'query_var'           => true,
			'capability_type'     => 'page',
			'menu_icon'  		  => 'dashicons-screenoptions',
		);

		register_post_type( 'offer', $args );
	}
}