<?php

class KreacjaExtras
{
    public function __construct()
    {
        $this->registerActions();
        $this->registerFilters();
    }

    protected function registerActions()
    {
        add_action( 'edit_category', [$this, 'categoryTransientFlusher']);
		add_action( 'save_post', [$this, 'categoryTransientFlusher']);
		add_action( 'user_register', [$this, 'hideUserDashboardWidgets']);
    }

    protected function registerFilters()
    {
       	add_filter( 'tiny_mce_before_init', [$this, 'TinyMCE_beforeInitInsertFormats']);
		add_filter( 'the_generator', [$this, 'removeWpVer']);
	}

	public function TinyMCE_beforeInitInsertFormats($init_array)
	{
		$selectors = 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,span';
		$style_formats = [
			[
				'title' => __('Style napisów', 'nazwa'),
				'items' => [
					['title' => __('duże napisy', 'nazwa'), 'selector' => $selectors, 'classes' => 'duze-napisy'],
					['title' => __('średnie napisy', 'nazwa'), 'selector' => $selectors, 'classes' => 'srednie-napisy'],
					['title' => __('małe napisy', 'nazwa'), 'selector' => $selectors, 'classes' => 'male-napisy'],
					['title' => __('nagłówek sekcji', 'nazwa'), 'selector' => $selectors, 'classes' => 'section-title'],
				]
				],
			[
				'title' => __('Przyciski', 'nazwa'),
				'items' => [
					['title' => __('Przycisk', 'nazwa'), 'selector' => 'a', 'classes' => 'button'],
				]
			]
		];
		$init_array['style_formats'] = json_encode( $style_formats );

		return $init_array;
	}

	public function categoryTransientFlusher()
	{
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}

		delete_transient( 'nazwa_categories' );
	}

	public function removeWpVer()
	{
		return '';
	}

	public function hideUserDashboardWidgets($user_id) {
		update_user_meta( $user_id, 'metaboxhidden_dashboard', ['dashboard_right_now', 'dashboard_activity', 'wpseo-dashboard-overview', 'dashboard_quick_press', 'dashboard_primary']);
		update_user_meta( $user_id, 'meta-box-order_dashboard', [
			'normal' => 'dashboard_right_now,dashboard_activity,kreacja_dashboard_pomoc,kreacja_dashboard_porady,wpseo-dashboard-overview',
			'side' => 'dashboard_quick_press,dashboard_primary,kreacja_dashboard_cms,kreacja_dashboard_kreacja',
			'column3' => '',
			'column4' => '',
		]);
	}
}
