<?php 

class KreacjaTaxonomies
{
	public function __construct()
	{
		add_action( 'init', [$this, 'registerTaxonomies']);
	}

	public function registerTaxonomies()
	{
		$this->registerProductsCategoriesTax();
	}

	private function registerProductsCategoriesTax()
	{
		$labels = array(
			'name'                       => _x( 'Kategorie oferty', 'Taxonomy General Name', 'nazwa' ),
			'singular_name'              => _x( 'Kategoria oferty', 'Taxonomy Singular Name', 'nazwa' ),
			'menu_name'                  => __( 'Kategorie oferty', 'nazwa' ),
			'all_items'                  => __( 'Wszystkie kategorie', 'nazwa' ),
			'parent_item'                => __( 'Parent Item', 'nazwa' ),
			'parent_item_colon'          => __( 'Parent Item:', 'nazwa' ),
			'new_item_name'              => __( 'kategoria', 'nazwa' ),
			'add_new_item'               => __( 'Dodaj kategorię', 'nazwa' ),
			'edit_item'                  => __( 'Edytuj', 'nazwa' ),
			'update_item'                => __( 'Zaktualizuj', 'nazwa' ),
			'view_item'                  => __( 'Zobacz kategorię', 'nazwa' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'nazwa' ),
			'add_or_remove_items'        => __( 'Dodaj lub usuń kategorię', 'nazwa' ),
			'choose_from_most_used'      => __( 'Wybierz najczęściej używaną', 'nazwa' ),
			'popular_items'              => __( 'Popularne kategorie', 'nazwa' ),
			'search_items'               => __( 'Szukaj', 'nazwa' ),
			'not_found'                  => __( 'Brak kategorii', 'nazwa' ),
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'rewrite' 					 => array('hierarchical'=>true),
			'show_tagcloud'              => true,
		);

		register_taxonomy( 'offer_categories', array( 'offer' ), $args );
	}
}