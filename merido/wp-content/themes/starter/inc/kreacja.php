<?php 

/*
 * Available modules: KreacjaAdmin, KreacjaAcf, KreacjaAjax, KreacjaShortcodes, KreacjaExtras, KreacjaPostTypes, KreacjaRodo, KreacjaTaxonomies, KreacjaWoocommerce
 * */
class Kreacja
{
	private $modules;

	public function __construct()
	{
		$this->modules = ['KreacjaAdmin', 'KreacjaAcf', 'KreacjaShortcodes', 'KreacjaExtras', 'KreacjaPostTypes', 'KreacjaRodo', 'KreacjaAjax', 'KreacjaWoocommerce'];

		$this->classLoader();
	}

	private function classLoader()
	{
		foreach ($this->modules as $module) {
			$name = $module.'.php';
			require get_template_directory() . '/inc/' . $name;

			$prop_name = strtolower($module);
			$class_name = $module;

			if (class_exists($class_name)) {
				$this->$prop_name = new $class_name();
			}
		}
	}
}

$kreacja = new Kreacja();