<?php 

class KreacjaAdmin
{
    public function __construct()
    {
        $this->registerActions();
        $this->registerFilters();
    }

    protected function registerActions()
    {
        add_action( 'login_enqueue_scripts', [$this, 'loadLoginStyles']);
        add_action( 'admin_enqueue_scripts', [$this, 'loadAdminStyles']);
    }

    protected function registerFilters()
    {
    }

    public function loadLoginStyles() 
    { 
        wp_enqueue_style('login-css', get_template_directory_uri() . '/assets/css/login.css', array(), '1.1', 'all');
    }

    public function loadAdminStyles() 
    {
        wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/assets/css/admin.css' );
    }
}    