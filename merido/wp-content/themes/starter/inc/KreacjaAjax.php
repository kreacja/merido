<?php


class KreacjaAjax
{
    function __construct()
    {
        $this->addAjaxCallback('ajaxSearch', [$this, 'ajaxSearch']);
        $this->addAjaxCallback('ajaxAddToCart', [$this, 'ajaxAddToCart']);
    }

    protected function addAjaxCallback($action_name, $callback)
    {
        add_action("wp_ajax_{$action_name}", $callback);
        add_action("wp_ajax_nopriv_{$action_name}", $callback);
    }

    public function ajaxSearch ()
    {
        $args = [
            'post_type' => 'product',
            'posts_per_page' => -1,
            's' => $_POST['s']
        ];
        $q = new WP_Query($args);
        $posts = [];

        if($q->have_posts()) {
            while ($q->have_posts()) : $q->the_post();
               $posts[] = [
                   "id" => get_the_ID(),
                   "name" => get_the_title(),
                   "link" => get_the_permalink()
               ];
            endwhile;
            
            wp_reset_postdata();
            wp_send_json_success($posts);
        } else {
            wp_send_json_error( "Something went wrong!" );
        }
    }

    public function ajaxAddToCart()
    {
        $product_id = $_POST['product_id'];
        $qty = 1;
        if($_POST['quantity']) {
            $qty = $_POST['quantity'];
        }

        $variation_id = null;
        if (isset($_POST['variation_id'])) {
            $variation_id = $_POST['variation_id'];
        }

        WC()->cart->add_to_cart($product_id, $qty, $variation_id);
        wp_send_json_success();
    }
}

new KreacjaAjax();
