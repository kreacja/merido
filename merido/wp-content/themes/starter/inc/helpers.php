<?php

if (!function_exists('post_posted_on')) {

	function post_posted_on() 
	{
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) )
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		echo '<span class="posted-on">' . $time_string . '</span>';
	}
	
}


function term_has_children($term, $taxonomy) 
{
	$children = get_term_children($term,$taxonomy);
	
	if (!empty($children))
		return true;

	return false;
}


function get_blog_title()
{
	if (get_option( 'page_for_posts' ))
		$title =  get_the_title( get_option( 'page_for_posts' ));
	else
		$title = __('Blog','nazwa');

	return $title;
}


function the_page_title() 
{
	$title = '';

	if (is_singular('post')){
		$title = get_the_category();
		$title = $title[0]->cat_name;
	} elseif (is_page() || is_singular()) 
		$title = get_the_title();
	elseif (is_home())
		$title = get_blog_title();
	elseif (is_category())
		$title = sprintf(__('%s','nazwa'),'<span>'.single_cat_title('',false).'</span>');
	elseif (is_tag())
		$title = sprintf(__('Tag: %s','nazwa'),'<span>'.single_cat_title('',false).'</span>');
	elseif (is_search())
		$title = sprintf( __( 'Wyniki dla: %s','nazwa'), '<span>' . get_search_query() . '</span>' );
	elseif (is_archive()){

		if (is_author())
			$title =  sprintf(__('Autor: %s','nazwa'),'<span>'.get_the_author().'</span>');
		elseif (is_day())
			$title = get_the_date();
		elseif (is_month())
			$title = get_the_date( 'F Y' );
		elseif (is_year())
			$title = get_the_date( 'Y' );
		elseif (is_tax()) {
			$tax = get_taxonomy(get_query_var('taxonomy'));

			if ($tax->hierarchical == false)
				$title = $tax->labels->singular_name.': '. single_term_title('',false);
			else
				$title = single_term_title('',false);
		} elseif (is_post_type_archive())
			$title = post_type_archive_title('',false);	
		else
			$title = __( 'Archiwum','nazwa');

	} elseif (is_404())
		$title = __('Nie znaleziono strony','nazwa');
	else
			$title = get_the_title();

	echo apply_filters('the_post_title',$title);
}


function pretty_pagination() 
{
	global $wp_query;

	if ($wp_query->max_num_pages >= 2) {
		$big = 999999999; 
		$pagination_args = array(
			'show_all'     => False,
			'end_size'     => 1,
			'mid_size'     => 2,
			'prev_next'    => True,
			'prev_text'    => '&laquo;',
			'next_text'    => '&raquo;',
			'type'         => 'plain',
			'add_args'     => False,
			'add_fragment' => '',
			'before_page_number' => '',
			'after_page_number' => ''
		);
		$pagination_args = apply_filters( 'pretty_pagination_params', $pagination_args );
		$before_pagination = apply_filters('pretty_before_pagination','<div class="pagination">');
		$after_pagination = apply_filters('pretty_after_pagination','</div>');

		echo $before_pagination . paginate_links( $pagination_args ) . $after_pagination;
	}
}


function outputHtml(&$value) 
{
	$value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}


function dump($var, $clear = false, $public = false) 
{
	$dump_ips_allowed = array(
		'127.0.0.1',
		'::1',
		'79.190.142.113'
	);

	if ($clear) 
		array_walk_recursive($var, 'outputHtml');

	if ($public || in_array(stripslashes($_SERVER['REMOTE_ADDR']), $dump_ips_allowed)) {
		echo '<pre style="background:cyan;padding:10px;z-index:1000000;color:#000;font-size:13px;">';
			print_r($var);
		echo '</pre>';
	}
}


function dd($var, $clear, $public)
{
	dump($var, $clear, $public);
	die();
}


function get_excerpt($count = 150)
{
	if (has_excerpt()){
		$excerpt = get_the_excerpt();
	} else {
		$excerpt = get_the_content();
		$excerpt = strip_tags($excerpt);
		$excerpt = strip_shortcodes( $excerpt );
		$excerpt = substr($excerpt, 0, $count);
		$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  		$excerpt = $excerpt.'...';
	}

	return $excerpt;
}


function replace_ellipsis($content) 
{
	$content = str_replace('&#8230;', '...', $content);

	return $content;
}
add_filter('the_content', 'replace_ellipsis', 30);


function print_filters_for($hook = '')
{
    global $wp_filter;

    if( empty( $hook ) || !isset( $wp_filter[$hook] ) )
        return;

    print '<pre>';
    print_r( $wp_filter[$hook] );
    print '</pre>';
}


function load_template_part($template, $name = '', $data = array()) 
{
	ob_start();
	do_action( "get_template_part_{$template}", $template, $name );
 	
    $templates = array();
    $name = (string) $name;

    if ( '' !== $name )
        $templates[] = "{$template}-{$name}.php";

    $templates[] = "{$template}.php";
	extract($data);
	include(locate_template($templates, false, false));
	$part = ob_get_contents();

	ob_end_clean();

	return $part;
}

function displaySingleProductPrice($product)
{
	if ($product->get_sale_price() > 0) : ?>
		<div class="old">
			<?php echo wc_price($product->get_regular_price()); ?>
		</div>
	<?php endif; ?>
	<div>
		<?php echo wc_price($product->get_price()); ?>
	</div>
<?php } 

// This function checks if there is a default variation set.
// If yes its price is displayed on the roducts list, otherwise the minimum variation price is displayed.
function displayVariablePrice($product)
{
	$available_variations = $product->get_available_variations();
	$selectedPrice = null;
 
	$defaultArray = array();
	foreach ($product->get_default_attributes() as $key => $val) {
		$defaultArray['attribute_' . $key] = $val;
	}

	$prices = array();
    foreach ($available_variations as $variation) {
		if (empty($defaultArray)) {
			$prices[] = $variation['display_price'];
		} else {
			$result = array_diff($defaultArray, $variation['attributes']);
			if (empty($result)) {
				$price = $variation['display_price'];
			}
		}
    }

	if (!empty($prices)) {
		$price = min($prices);
	}

    $selectedPrice = wc_price($price);

    return $selectedPrice;
}