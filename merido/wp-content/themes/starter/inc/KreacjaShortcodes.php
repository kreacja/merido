<?php

class KreacjaShortcodes
{
    public $registered_shortcodes;

    public function __construct()
    {
		$this->registered_shortcodes = [];

        $this->registerActions();
    }

    protected function registerActions()
    {
        add_action('init', [$this, 'registerShortcodes'], 9999);
    }

    public function addShortcode($name, $callback)
    {
        add_shortcode($name, $callback);
        $this->registered_shortcodes[] = $name;
    }

    public function registerShortcodes()
    {
        $this->addShortcode('display-categories-menu', array($this, 'displayCategoriesMenu'));
        $this->addShortcode('display-archive-with-sidebar', array($this, 'displayArchiveWithSidebar'));
        $this->addShortcode('display-archive-without-sidebar', array($this, 'displayArchiveWithoutSidebar'));
        $this->addShortcode('display-recommended-products', array($this, 'displayRecommendedProducts'));
        $this->addShortcode('extended-add-to-cart', array($this, 'extendedAddToCart'));
        $this->addShortcode('simple-add-to-cart', array($this, 'simpleAddToCart'));
        $this->addShortcode('display-newsletter-form', array($this, 'displayNewsletterForm'));
        $this->addShortcode('display-categories-tiles', array($this, 'displayCategoriesTiles'));
    }

    public function displayCategoriesTiles($atts)
    {
        return load_template_part('/template-parts/categories-tiles', null, array('shortcode_data' => $atts));
    }

    public function displayCategoriesMenu($atts)
    {
        return load_template_part('/template-parts/categories-menu', null, array('shortcode_data' => $atts));
    }

    public function displayArchiveWithSidebar($atts)
    {
        return load_template_part('/template-parts/woocommerce/archive-product-with-sidebar', null, array('shortcode_data' => $atts));
    }

    public function displayArchiveWithoutSidebar($atts)
    {
        return load_template_part('/template-parts/woocommerce/archive-product-without-sidebar', null, array('shortcode_data' => $atts));
    }

    public function displayRecommendedProducts($atts) 
    {
        return load_template_part('/template-parts/woocommerce/recommended-products-list', null, array('shortcode_data' => $atts));
    }

    public function extendedAddToCart($atts)
    {
        return load_template_part('/template-parts/woocommerce/add-to-cart-extended', null, array('shortcode_data' => $atts));
    }

    public function simpleAddToCart($atts)
    {
        return load_template_part('/template-parts/woocommerce/add-to-cart-simple', null, array('shortcode_data' => $atts));
    }

    public function displayNewsletterForm($atts)
    {
        return load_template_part('/template-parts/newsletter-form', null, array('shortcode_data' => $atts));
    }
}
