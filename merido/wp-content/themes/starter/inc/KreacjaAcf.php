<?php 

class KreacjaAcf
{
	public function __construct()
	{
		$this->addAcfOptionsPage();
	}

	protected function addAcfOptionsPage()
	{
		if (function_exists('acf_add_options_page')) {
			acf_add_options_sub_page(array(
				'page_title' 	=>  __('Ustawienia motywu', 'nazwa'),
				'menu_title'	=>  __('Ustawienia motywu', 'nazwa'),
				'parent_slug'	=> 'themes.php',
			));
		}
	}
}
