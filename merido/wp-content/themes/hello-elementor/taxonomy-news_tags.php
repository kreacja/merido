<?php get_header(); ?>
	<?php
$term = get_queried_object();
$cat__name =$term->name; 
  ?>
<h1 class='news__header'>MERIDOnews.</h1>
<?php

$the_query = new WP_Query( array('posts_per_page'=>6,
'post_type'=>'news',
'tax_query' => array(
  array (
      'taxonomy' => 'news_tags',
      'field' => 'slug',
      'terms' => $cat__name,
  )
),
'paged' => get_query_var('paged') ? get_query_var('paged') : 1),

); 
?>
<div class='news__wrapper'>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
<div class='single__post'>
<div class='post__thumbnail'>
<a href='<?php the_permalink(); ?>'>
<?php the_post_thumbnail(); ?>
</a>
</div>
<div class='post__content'>
  <h2><?php echo get_the_title(); ?></h2>
  <div class='post__date'><img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar.svg"/><?php 	
$post_date = get_the_date( 'F j Y' ); echo $post_date; ?></div>
  <div class='post__desc'><?php the_content(); ?></div>
  <a class='post__link' href="<?php the_permalink(); ?>">Czytaj więcej</a>
</div>
</div>

<?php
endwhile; ?>
</div>
<div class='pagination'> <?php

$big = 999999999; // need an unlikely integer
echo paginate_links( array(
'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
'format' => '/page/%#%',
'current' => max( 1, get_query_var('paged') ),
'mid_size'      => 1,
'end_size'      => 2,
'total' => $the_query->max_num_pages,
) );

?> </div> <?php

wp_reset_postdata();
?>
<?php echo do_shortcode( '[elementor-template id="3778"]' ); ?>

<?php get_footer(); ?>