
<?php ob_start(); ?>
<?php get_header(); ?>
<h1 class='news__header'>MERIDOnews.</h1>
<?php
$file = get_field('pobierz');
?>

<section class='post'>
  <div class='post__cont'>
  <h2><?php the_title(); ?></h2>
  <div class='post__date'><img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar.svg"/><?php the_date(); ?></div>
  
  <p><?php 
  ob_end_flush();

  echo do_shortcode( "[dkpdf-button]" ); ?></p>
  <div class='content'><?php echo the_field('tekst__wpisu') ?></div>
  </div>
  <div class='post__data'>
    <div class='category__list'>
      <h4>Kategorie</h4>

      <?php
// Get the taxonomy's terms
$terms = get_terms(
  array(
      'taxonomy'   => 'news_category',
      'hide_empty' => true,
  )
);

// Check if any term exists
if ( ! empty( $terms ) && is_array( $terms ) ) {
  // Run a loop and print them all
  foreach ( $terms as $term ) { ?>
      <a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
          <?php echo $term->name; ?>
          <div class='underline'></div>
      </a><?php
  }
} 
?>



    </div>
    <div class='tags__list'>
    <h4>Tagi</h4>
    <div>
    <?php
    
// Get the taxonomy's terms
$terms = get_terms(
  array(
      'taxonomy'   => 'news_tags',
      'hide_empty' => true,
  )
);

// Check if any term exists
if ( ! empty( $terms ) && is_array( $terms ) ) {
  // Run a loop and print them all
  foreach ( $terms as $term ) { ?>
      <a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
          <?php echo $term->name; ?>
      </a><?php
  }
} 
?>
</div>
    </div>
  </div>
</section>

<?php get_footer(); ?>